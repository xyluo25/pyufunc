.. _user_guide:

==========
User Guide
==========

The User Guide covers all of pyufunc by topic area.
Further information on any specific method can be obtained in the
:ref:`api`.

Guides
-------

