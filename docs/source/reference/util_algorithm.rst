
.. _api.util_algo:

===============
util_algorithm
===============
.. currentmodule:: pyufunc

Sorting Algorithm
~~~~~~~~~~~~~~~~~
.. autosummary::
    :toctree: api/

    bubble_sort
    heap_sort
    insertion_sort
    merge_sort
    quick_sort
    selection_sort

