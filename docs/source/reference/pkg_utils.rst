.. _api.pkg_utils:

=========
pkg_utils
=========
.. currentmodule:: pyufunc

Utilities
~~~~~~~~~
.. autosummary::
   :toctree: api/

   import_package
   func_running_time
   func_time
   requires
   get_user_defined_func
   is_user_defined_func
   is_module_importable
   run_parallel
   end_of_life
