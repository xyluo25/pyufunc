.. _api.util_office:

================
util_office
================
.. currentmodule:: pyufunc

Email
~~~~~~~~~~~
.. autosummary::
   :toctree: api/

   is_valid_email
   send_email
