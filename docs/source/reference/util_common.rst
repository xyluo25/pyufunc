
.. _api.util_common:

===========
util_common
===========
.. currentmodule:: pyufunc

docstring style
~~~~~~~~~~~~~~~
.. autosummary::
   :toctree: api/

   show_docstring_headers
   show_docstring_google
   show_docstring_numpy

password_generator
~~~~~~~~~~~~~~~~~~
.. autosummary::
   :toctree: api/

   generate_password
