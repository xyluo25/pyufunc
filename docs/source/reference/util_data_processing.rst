
.. _api.util_data_processing:

====================
util_data_processing
====================
.. currentmodule:: pyufunc

list manipulation
~~~~~~~~~~~~~~~~~
.. autosummary::
   :toctree: api/

   split_list_by_equal_sublist
   split_list_by_fixed_length

dictionary manipulation
~~~~~~~~~~~~~~~~~~~~~~~
.. autosummary::
   :toctree: api/

   split_dict_by_chunk

common data processing
~~~~~~~~~~~~~~~~~~~~~~
.. autosummary::
    :toctree: api/

    cvt_int_to_alpha
